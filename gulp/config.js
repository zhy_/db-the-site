const path = require('path');
const srcDir  = path.resolve(__dirname, "../src");

const config = {
  baseDir: './dist',
  paths: {
    project: './dist',
    sass: {
      src: './src/sass',
      all: './src/sass/**/*.sass',
      dest: './dist/css'
    },
    js: {
      src: './src/js',
      all: './src/js/**/*.js',
      dest: './dist/js',
      entry: {
        'master': ['babel-polyfill', `${srcDir}/js/index.js`],
      }
    },
    templates: {
      src: './src',
      all: './src/**/*.html',
      dest: './dist'
    },
    images: {
      src: './src',
      all: './src/images/**/*',
      dest: './dist/images'
    }
  },
  serverOptions : {
    server: {
      baseDir: './dist',
    },
    files: [
      './dist/css/**/*.css',
      './dist/js/**/*.js',
      './dist/**/*.html'
    ],
    injectChanges: true,
		notify: {
	    styles: {
    		top: 'auto',
    		bottom: '0'
	    }
		}
  }
};

module.exports = config;
