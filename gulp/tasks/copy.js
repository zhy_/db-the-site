const gulp = require('gulp');
const config = require('../config');


/**
*
 * Builds sass
 *
 * Command: gulp copy:html
 *
 */

 gulp.task('copy:html', function (done) {
     gulp.src(config.paths.templates.all)
         .pipe(gulp.dest(config.paths.templates.dest));
     done();
 });
