const gulp = require('gulp');
const config = require('../config');
const browserSync = require('browser-sync').create();

/*
 *
 * Development
 *
 * Commands: gulp
 *
*/

gulp.task('default', gulp.series('sass:dev', 'js', 'images', 'copy:html', 'server', (done) => {
	gulp.watch(config.paths.sass.all, gulp.series('sass:dev'));
	gulp.watch(config.paths.js.all, gulp.series('js'));
	gulp.watch(config.paths.templates.all, gulp.series('copy:html'));
	gulp.watch(config.paths.images.all, gulp.series('images'));
  done();
}));
