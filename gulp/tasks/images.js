const gulp = require('gulp');
const config = require('../config');
const imagemin = require('gulp-imagemin');

gulp.task('images', (done) => {
    gulp.src(config.paths.images.all)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(config.paths.images.dest))
    done();
});
