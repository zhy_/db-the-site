const gulp = require('gulp');
const babel = require('gulp-babel');
const config = require('../config');
const webpack = require('webpack-stream');
const webpackConfig = require('../../webpack.config');

/**
*
 * Builds dev scripts
 *
 * Command: gulp js
 *
 */

gulp.task('js', (done) => {
  gulp.src(config.paths.js.all)
      .pipe(webpack(webpackConfig))
      .pipe(gulp.dest(config.paths.js.dest));
  done();
});
