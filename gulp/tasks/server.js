const gulp = require('gulp');
const config = require('../config');
const browserSync = require('browser-sync').create();

/*
 *
 * Development
 *
 * Commands: gulp server
 *
*/

gulp.task('server', gulp.series((done) => {
  browserSync.init(config.serverOptions);
  done();
}));
// gulp.task('server', function() {
//   browserSync.init(config.serverOptions);
//   browserSync.watch(config.paths.sass.all, ['sass']);
// 	browserSync.watch(config.paths.templates.all, ['copy:html']);
// 	browserSync.watch(config.paths.images.all, ['images']);
// });
