const gulp = require('gulp');
const config = require('../config');
const browserSync = require('browser-sync').create();

const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const postcss = require('gulp-postcss');
const purify = require('gulp-purifycss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const globImporter = require('node-sass-glob-importer');


/**
 * Builds sass
 *
 * Command: gulp sass:prod
 *
 * @param {}
 *
 * @returns {}
 */

const postcssPluginsDev = [
 autoprefixer({
   browsers: ['>1%', 'last 2 versions', 'ie >= 9']
 })
];
const postcssPluginsProd = [
 ...postcssPluginsDev,
 cssnano()
];

gulp.task('sass:dev', (done) => {
  const stream = gulp.src(['**/*.sass'], {cwd: config.paths.sass.src})
		.pipe(sassLint())
		.pipe(sassLint.format())
    .pipe(sass({
    	importer: globImporter()
  	})
		.on('error', sass.logError))
    .pipe(postcss(postcssPluginsDev))
    .pipe(gulp.dest(config.paths.sass.dest));

  done();
  return stream;
});


gulp.task('sass:prod', (done) => {
  const stream = gulp.src(['**/*.sass'], {cwd: config.paths.sass.src})
		.pipe(sassLint())
		.pipe(sassLint.format())
    .pipe(sassLint.failOnError())
		.pipe(sass({
    	importer: globImporter()
  	})
		.on('error', sass.logError))
    .pipe(purify([config.paths.js.all, config.paths.templates.all]))
    .pipe(postcss(postcssPluginsProd))
    .pipe(gulp.dest(config.paths.sass.dest));

  done();
  return stream;
});
