const gulp = require('gulp');
const gutil = require('gulp-util');
const copy = require('./gulp/tasks/copy');
const sass = require('./gulp/tasks/styles');
const scripts = require('./gulp/tasks/scripts');
const server = require('./gulp/tasks/server');
const images = require('./gulp/tasks/images');
const production = require('./gulp/tasks/production');
const main = require('./gulp/tasks/default');
