# The Site

### Installation
`npm install` to install all modules and dependencies.

### Tasks

Task | Description
--- | --- | ---
`npm run dev` | development with local server
`npm run build:dev` | build development
`npm run build:prod` | build production
