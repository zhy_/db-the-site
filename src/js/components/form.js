export default class {
  constructor() {
    this.form = document.querySelector('#form');
    this.formSubmit = document.querySelector('#submit');
    this.inputFirstname = document.querySelector('#firstname');
    this.inputLastname = document.querySelector('#lastname');
    this.inputMessage = document.querySelector('#message');
    this.errorsWrap = document.querySelector('[data-form-errors]');
    this.hasErrors = [];
    this.bindings();
  }

  bindings = () => {
    this.form.addEventListener('submit', this.formSubmitF);
  }

  formSubmitF = (e) => {
    e.stopPropagation();

    const checkInputArr = [this.inputFirstname, this.inputLastname, this.inputMessage];

    if (this.checkEmpty(checkInputArr)) {
      e.preventDefault();
    }
  }

  checkIsEmptyElement = (el) => {
    const elID = el.getAttribute('id');

    if (el.value === '' || el.value === el.defaultValue) {
      if (!this.checkIfElHasError(elID)) {
        this.createErrorEl(elID);
      }
    } else {
      if (this.checkIfElHasError(elID)) {
        this.removeErrorEl(elID);
      }
    }
  }

  checkEmpty = (els) => {
    els.forEach(this.checkIsEmptyElement);
    return !!Object.keys(this.hasErrors).length;
  }

  createErrorEl = (elID) => {
    const el = document.getElementById(elID);
    const errorEl = document.createElement('div');
    const errorText = document.createTextNode(el.getAttribute('data-error-text'));
    errorEl.classList.add(`form-error--${elID}`);
    errorEl.appendChild(errorText);
    this.errorsWrap.appendChild(errorEl);
    document.getElementById(elID).parentElement.classList.add('form-block--error');
    this.hasErrors.push(elID);
  }

  checkIfElHasError = elID => this.hasErrors.indexOf(elID) !== -1;

  removeErrorEl = (elID) => {
    this.errorsWrap.querySelector(`.form-error--${elID}`).remove();
    this.hasErrors.splice(this.hasErrors.indexOf(elID), 1);
    document.getElementById(elID).parentElement.classList.remove('form-block--error');
  }
}
