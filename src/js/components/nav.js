export default class {
  constructor() {
    this.navToggle = document.querySelector('[data-nav-toggle]');
    this.bindings();
  }

  bindings = () => {
    this.navToggle.addEventListener('click', this.navToggleF);
  }

  navToggleF = (e) => {
    e.preventDefault();
    e.stopPropagation();
    document.querySelector('body').classList.toggle('nav--mobile--opened');
  }
}
