const path = require('path');
const webpack = require('webpack');
const SRC_DIR  = path.resolve(__dirname, "./src");
const DIST_DIR = path.resolve(__dirname, "./dist");
const config = require('./gulp/config');

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  entry: config.paths.js.entry,
  output: {
    path: DIST_DIR,
    filename: '[name].js',
    publicPath: "/dist"
  },
  module: {
    rules: [{
      enforce: "pre",
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use:[{
        loader: "eslint-loader",
      }]
    },
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: [
        'babel-loader'
      ]
    }]
  },
  context: SRC_DIR,
  resolve: {
    extensions: ['.js', '.json'],
    alias: {
      '@': resolve('src'),
    },
    modules: ["node_modules"],
  },
  devtool: '#eval-source-map',
  watch: true
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';
  module.exports.watch = false;
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
